from typing import Optional
from fastapi import FastAPI, Response, status
import uvicorn
import uuid
from sqlalchemy.orm import Session
import aiosqlite
from pydantic import BaseModel


app = FastAPI()

class People(BaseModel):
    uuid: int
    survived: bool
    passengerClass: int
    name: str
    sex: str
    age: int
    siblingsOrSpousesAboard: int
    parentsOrChildrenAboard: int
    fare: float



@app.get('/')
async def index():
    return{'key':'value'}

@app.on_event("startup")
async def startup():
    app.db_connection = await aiosqlite.connect('titanic.db')

@app.get("/people")
async def get_tracks(page: int = 0, per_page: int = 10):
    app.db_connection.row_factory = aiosqlite.Row
    cursor = await app.db_connection.execute("SELECT * FROM titanic ORDER BY name LIMIT :per_page OFFSET :per_page*:page",
        {'page': page, 'per_page': per_page})
    people = await cursor.fetchall()
    return people


@app.post("/people")
async def add_people(response: Response, people: People):
    cursor = await app.db_connection.execute("SELECT name FROM titanic WHERE name = :name",
                                             {'artist_id': people.name})
    result = await cursor.fetchone()

    if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"detail": {"error": "Person was not in Titanic."}}

    cursor = await app.db_connection.execute("INSERT INTO titanic (survived, passengerClass, name, sex, age, "
                                             "siblingsOrSpousesAboard, "
                                             "parentsOrChildrenAboard, fare) VALUES (: survived, :passengerClass, :name, :sex, :age, :siblingsOrSpousesAboard, :parentsOrChildrenAboard, :fare)",
                                             {'survived': people.survived, 'passengerClass': people.passengerClass, 'name':people.name,
                                              'sex':  people.sex, 'age':  people.age, 'siblingsOrSpousesAboard':  people.siblingsOrSpousesAboard ,
                                              'parentsOrChildrenAboard':  people.parentsOrChildrenAboard, 'fare':  people.fare})



    await app.db_connection.commit()
    response.status_code = status.HTTP_201_CREATED
    return {'survived': people.survived, 'passengerClass': people.passengerClass, 'name':people.name,
                                              'sex':  people.sex, 'age':  people.age, 'siblingsOrSpousesAboard':  people.siblingsOrSpousesAboard ,
                                              'parentsOrChildrenAboard':  people.parentsOrChildrenAboard, 'fare':  people.fare}



@app.get("/people/{uuid}")
async def get_album(uuid: int):
    app.db_connection.row_factory = aiosqlite.Row
    cursor = await app.db_connection.execute("SELECT * FROM titanic WHERE uuid = :uu_id",
        {'uid': uuid})
    person = await cursor.fetchone()
    return person



@app.put("/people/{uuid}")
async def put_customer(response: Response, uuid: int, people: People):
    cursor = await app.db_connection.execute("SELECT uuid FROM titanic WHERE uuid = :uu_id",
        {"uuid": uuid})
    result = await cursor.fetchone()
    if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"detail": {"error": "UUID doesnt exist with that id does not exist."}}

    person = {k: v for k, v in people.dict().items() if v is not None}
    for key, value in person.items():
        cursor = await app.db_connection.execute("UPDATE titanic SET " + f"{key}" +" = :value WHERE uuid = :uu_id",
        {"customer_id": uuid, "value": value})
        await app.db_connection.commit()

    app.db_connection.row_factory = aiosqlite.Row
    cursor = await app.db_connection.execute("SELECT * FROM customers WHERE uuid = :uuid",
        {"uuid": uuid})
    person = await cursor.fetchone()
    return person

if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=5001)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
