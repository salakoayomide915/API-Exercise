This documentation is for an HTTP rest API with endpoints.

This API application was developed from scratch using FastAPI and it was tested on the localhost using uvicorn server.
The endpoints of this API can be tested using postman or insomnia.

The workflow of this mini project is as follows