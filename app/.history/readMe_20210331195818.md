This documentation is for an HTTP rest API with endpoints.

This API application was developed from scratch using FastAPI and it was tested on the localhost using uvicorn server.
The endpoints of this API can be tested using postman or insomnia.

The workflow of this mini project is as follows:
 - the .csv file provided in the instruction was imported into sqlite database.table and was used to create a titanic.db file
 - the main.py file contains the framework of the api and fastapi was used. requirements.txt holds all the tools used to build the API and sync it to the database
 - The Dockerfile was created and was deployed in mimikube, locally using the below steps-



