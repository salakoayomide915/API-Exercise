This documentation is for an HTTP rest API with endpoints.

This API application was developed from scratch using FastAPI and it was tested on the localhost using uvicorn server.
The endpoints of this API can be tested using postman or insomnia.

The workflow of this mini project is as follows:
 - the .csv file provided in the instruction was imported into sqlite database.table and was used to create a titanic.db file
 - the main.py file contains the framework of the api and fastapi was used. requirements.txt holds all the tools used to build the API and sync it to the database
 - The Dockerfile was created and was deployed in mimikube, locally using the below steps:-


 After tagging the image properly, from the last instructions in the Dockerfile

# 
 docker push localhost:5000/ubuntu


# Start minikube
minikube start

# Set docker env
eval $(minikube docker-env)

# Build image
docker build -t foo:0.0.1 .

# Run in minikube
kubectl run hello-foo --image=foo:0.0.1 --image-pull-policy=Never

# Check that it's running
kubectl get pods