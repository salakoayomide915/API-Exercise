This documentation is for an HTTP rest API with endpoints.

This API application was developed from scratch using FastAPI and it was tested on the localhost using uvicorn server.
The endpoints of this API can be tested using postman or insomnia.

The workflow of this mini project is as follows:
 - the .csv file provided in the instruction was imported into sqlite database.table and was used to create a titanic.db file
 - the main.py file contains the framework of the api and fastapi was used. requirements.txt holds all the tools used to build the API and sync it to the database
 - The Dockerfile was created and was deployed in mimikube, locally using the below steps:-


 After tagging the image properly, from the last instructions in the Dockerfile

# push the image locally and use the OS type of your local machine as an identifier
 docker push localhost:5000/ubuntu

# confirm that you are able to pull it
docker pull localhost:5000/ubuntu

# Start minikube
minikube start

# Set docker env
eval $(minikube docker-env)

# Build image
docker build -t myimage:0.0.1 .

# Run in minikube; the image-pull-policy is to ensure that minikube does not download the docker image automatically whenever it is run 

kubectl run hello-myimage --image=myimage:0.0.1 --image-pull-policy=Never

# Check that it's running
kubectl get pods




we can use terraform to automate the creation of kubernetes clusters to a cloud service provider and i provided a possible mockup in the root folder, using AWS 