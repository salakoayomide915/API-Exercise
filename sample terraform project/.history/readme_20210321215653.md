# Create a Terraform project with one ECR repo and one EKS cluster


# Since this is a demo project, we can run this test in a docker container instead of downloading so, Run Amazon CLI

docker run -it --rm -v ${PWD}:/work -w /work --entrypoint /bin/sh amazon/aws-cli:2.0.43



# we need to install some tools for downloads, unzipping and editing in the container spun up

yum install -y jq gzip nano tar git unzip wget



```

## Login to AWS
```

# Access your "My Security Credentials" section in your profile. 

# Create an access key

aws configure

Default region name: ap-southeast-2

Default output format: json


# Download the Terraform CLI in the container using its web link, unzip it, then make it executable
curl -o /tmp/terraform.zip -LO https://releases.hashicorp.com/terraform/0.13.1/terraform_0.13.1_linux_amd64.zip

unzip /tmp/terraform.zip

chmod +x terraform && mv terraform /usr/local/bin/

## The terraform project contains 3 files

main.tf
outputs.tf
variables.tf

## main.tf

this file would create the following resources:- 
    - one public and one private subnet (2 security groups)
    - one vpc
    - iam role with permissions 
    - EKS cluster
    - one worker group/ autoscaling group
    - one ECR
    - Your kubectl configuration


## Initialize terraform, download plugins, modules and provider information
terraform init

terraform validate

terraform plan

terraform apply

```

# Lets see what we deployed

```

# grab our EKS config

aws eks update-kubeconfig --name safebodaEKS --region ap-southeast-2


# Get kubectl

 -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-relecurlase/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x ./kubectl

mv ./kubectl /usr/local/bin/kubectl


kubectl get nodes

kubectl get deploy

kubectl get pods

kubectl get svc


```

# Clean up 

```
terraform destroy
```
